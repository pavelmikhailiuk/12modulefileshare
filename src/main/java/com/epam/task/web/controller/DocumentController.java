package com.epam.task.web.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.exception.FileServiceException;
import com.epam.task.service.DocumentService;

@Controller
@RequestMapping(value = "/files")
public class DocumentController {

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String UPLOAD_URL = "/upload";
	private static final String ID_URL = "/{id}";
	private static final String LOCATION_HEADER = "Location";

	@Autowired
	private DocumentService documentService;

	@PostMapping(value = UPLOAD_URL)
	public void uploadFile(@RequestParam(required = true) MultipartFile file, UriComponentsBuilder ucBuilder,
			HttpServletRequest request, HttpServletResponse response) throws FileServiceException, IOException {
		Document document = new Document(null, file.getOriginalFilename(), file.getBytes(), new Date());
		Long id = documentService.save(document);
		String url = request.getRequestURL().toString();
		response.addHeader(LOCATION_HEADER, url.substring(0, url.length() - (UPLOAD_URL.length() - 1)) + id);
		response.setStatus(HttpStatus.CREATED.value());
	}

	@GetMapping
	@ResponseBody
	public List<DocumentMetadata> findFiles(
			@RequestParam(required = true) @DateTimeFormat(pattern = DATE_FORMAT) Date date)
			throws FileServiceException {
		List<DocumentMetadata> filesInfo = documentService.findAllValid(date);
		return filesInfo;
	}

	@GetMapping(value = ID_URL)
	public ResponseEntity<byte[]> downloadFile(@PathVariable Long id) throws FileServiceException {
		Document document = documentService.find(id);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentDispositionFormData("attachment", document.getName());
		return new ResponseEntity<>(document.getData(), httpHeaders, HttpStatus.OK);
	}

	@DeleteMapping(value = ID_URL)
	public void deleteFile(@PathVariable Long id, HttpServletResponse response) throws FileServiceException {
		documentService.delete(id);
		response.setStatus(HttpStatus.NO_CONTENT.value());
	}
}
