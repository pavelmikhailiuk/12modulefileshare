package com.epam.task.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.exception.FileServiceException;
import com.epam.task.repository.DocumentRepository;
import com.epam.task.service.DocumentService;

@Service
@Transactional
public class DocumentServiceImpl implements DocumentService {

	@Autowired
	private DocumentRepository documentRepository;

	@Override
	public void delete(Long id) throws FileServiceException {
		if (id == null) {
			throw new FileServiceException("Id is null");
		}
		try {
			Document document = documentRepository.findOne(id);
			if (document != null) {
				documentRepository.delete(id);
			} else {
				throw new FileServiceException("Wrong id");
			}
		} catch (Exception e) {
			throw new FileServiceException("Error delete document", e);
		}
	}

	@Override
	public Document update(Document entity) throws FileServiceException {
		if (entity == null) {
			throw new FileServiceException("Document is null");
		}
		Document document = null;
		try {
			document = documentRepository.save(entity);
		} catch (Exception e) {
			throw new FileServiceException("Error update document", e);
		}
		return document;
	}

	@Override
	public Long save(Document entity) throws FileServiceException {
		if (entity == null) {
			throw new FileServiceException("Document is null");
		}
		Long id = null;
		try {
			Document document = documentRepository.save(entity);
			id = document != null ? document.getId() : null;
		} catch (Exception e) {
			throw new FileServiceException("Error save document", e);
		}
		return id;
	}

	@Override
	public Document find(Long id) throws FileServiceException {
		if (id == null) {
			throw new FileServiceException("Id is null");
		}
		Document document = null;
		try {
			document = documentRepository.findOne(id);
		} catch (Exception e) {
			throw new FileServiceException("Error delete document", e);
		}
		return document;
	}

	@Override
	public List<DocumentMetadata> findAllValid(Date expired) throws FileServiceException {
		if (expired == null) {
			throw new FileServiceException("Date expired is null");
		}
		List<DocumentMetadata> filesInfo = null;
		try {
			filesInfo = documentRepository.findAllValid(expired);
		} catch (Exception e) {
			throw new FileServiceException("Error find valid documents", e);
		}
		return filesInfo;
	}
}
