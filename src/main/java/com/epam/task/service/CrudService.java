package com.epam.task.service;

import com.epam.task.exception.FileServiceException;

public interface CrudService<T> {
	void delete(Long id) throws FileServiceException;

	T update(T entity) throws FileServiceException;

	Long save(T entity) throws FileServiceException;

	T find(Long id) throws FileServiceException;
}
