package com.epam.task.service;

import java.util.Date;
import java.util.List;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.exception.FileServiceException;

public interface DocumentService extends CrudService<Document> {
	List<DocumentMetadata> findAllValid(Date expired) throws FileServiceException;
}
