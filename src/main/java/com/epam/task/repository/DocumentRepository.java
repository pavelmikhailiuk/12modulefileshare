package com.epam.task.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;

public interface DocumentRepository extends CrudRepository<Document, Long> {

	@Query("select new com.epam.task.domain.DocumentMetadata(d.id, d.name) from Document d where d.loaded> :loaded")
	List<DocumentMetadata> findAllValid(@Param("loaded") Date loaded);
}
