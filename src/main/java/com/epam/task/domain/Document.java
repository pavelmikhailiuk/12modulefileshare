package com.epam.task.domain;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Document implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOC_SEQ")
	@SequenceGenerator(name = "DOC_SEQ", sequenceName = "DOC_SEQ", allocationSize = 1)
	private Long id;

	@Column(nullable = false)
	private String name;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(nullable = false)
	private byte[] data;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date loaded;

	public Document(Long id, String name, byte[] data, Date loaded) {
		this.id = id;
		this.name = name;
		this.data = data;
		this.loaded = loaded;
	}

	public Document() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Date getLoaded() {
		return loaded;
	}

	public void setLoaded(Date loaded) {
		this.loaded = loaded;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		result = prime * result + ((loaded == null) ? 0 : loaded.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Document other = (Document) obj;
		if (!Arrays.equals(data, other.data))
			return false;
		if (loaded == null) {
			if (other.loaded != null)
				return false;
		} else if (!loaded.equals(other.loaded))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Document [id=" + id + ", name=" + name + ", data=" + data.toString() + ", loaded=" + loaded + "]";
	}
}
