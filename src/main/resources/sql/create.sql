create user filedb identified by pass;
alter session set current_schema=filedb;
grant all privileges to filedb;

create sequence "DOC_SEQ" minvalue 1 maxvalue 999999999999999999999999999 increment by 1 start with 1;
create table DOCUMENT ("id" number(20,0) not null primary key, "name" NVARCHAR2(30) not null, "data" blob not null, "loaded" date not null);