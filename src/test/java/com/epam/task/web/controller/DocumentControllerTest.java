package com.epam.task.web.controller;

import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.service.DocumentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DocumentControllerTest {

	private static final String BASE_URL = "/files";
	private static final String UPLOAD_URL = "/upload";
	private static final String PARAM_DATE = "2016-09-10";
	private static final String PARAM_DATE_FORMAT = "yyyy-MM-dd";
	private static final String LOCATION_HEADER = "http://localhost/files/0";
	private static final MediaType CONTENT_TYPE = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private DocumentMetadata fileInfo;
	private String fileInfoString;
	private MockMultipartFile multipartFile;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DocumentService documentService;

	@Before
	public void setUp() {
		fileInfo = new DocumentMetadata(1L, "testfile.txt");
		fileInfoString = "[{\"id\":1,\"name\":\"testfile.txt\"}]";
		multipartFile = new MockMultipartFile("file", "test.txt", "text/plain", "Test file content".getBytes());
	}

	@Test
	public void uploadFileTest() throws Exception {
		this.mockMvc.perform(fileUpload(BASE_URL + UPLOAD_URL).file(multipartFile)).andExpect(status().isCreated())
				.andExpect(header().string("Location", LOCATION_HEADER));
		then(documentService).should().save(any(Document.class));
	}

	@Test
	public void downloadFileTest() throws Exception {
		Long id = 1L;
		when(documentService.find(id))
				.thenReturn(new Document(1L, "test.txt", "Test file content".getBytes(), new Date()));
		this.mockMvc.perform(get(BASE_URL + "/" + id)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
				.andExpect(content().string("Test file content"));
	}

	@Test
	public void findFilesTest() throws Exception {
		when(documentService.findAllValid(any(Date.class))).thenReturn(Arrays.asList(fileInfo));
		this.mockMvc.perform(get(BASE_URL).param("date", PARAM_DATE)).andExpect(status().isOk())
				.andExpect(content().contentType(CONTENT_TYPE)).andExpect(content().string(fileInfoString));
		SimpleDateFormat sdf = new SimpleDateFormat(PARAM_DATE_FORMAT);
		verify(documentService, times(1)).findAllValid(sdf.parse(PARAM_DATE));
	}

	@Test
	public void deleteFileTest() throws Exception {
		Long id = 1L;
		this.mockMvc.perform(delete(BASE_URL + "/" + id)).andExpect(status().isNoContent());
		verify(documentService, times(1)).delete(id);
	}
}
